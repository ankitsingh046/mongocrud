var passport = require('passport');
// require('../config/passport')(passport)
var exports = (module.exports = {});

exports.index = function (req, res) {
    res.render('index.ejs'); // load the index.ejs file
};

exports.login = function (req, res) {
    // render the page and pass in any flash data if it exists
    res.render('login.ejs', { message: req.flash('loginMessage') });
};

exports.signup = function (req, res) {
    // render the page and pass in any flash data if it exists
    res.render('signup.ejs', { message: req.flash('signupMessage') });
};

exports.profile = function (req, res) {
    res.render('profile.ejs', {
        user: req.user // get the user out of session and pass to template
    })
};
exports.admin = function (req, res) {
    console.log(req.user);
    if (req.user.userType == 'admin') {
        res.render('admin_dashboard.ejs', {
            user: req.user
        })
    } else {
        res.redirect('/profile')
    }
}
exports.logout = function (req, res) {
    req.logout();
    res.redirect('/');
};

exports.isLoggedIn=function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}
exports.passlogin =  passport.authenticate('local-login', {
    successRedirect: '/profile', // redirect to the secure profile section
    failureRedirect: '/login', // redirect back to the signup page if there is an error
    failureFlash: true // allow flash messages
});
exports.passsignup = passport.authenticate('local-signup', {
    successRedirect: '/profile', // redirect to the secure profile section
    failureRedirect: '/signup', // redirect back to the signup page if there is an error
    failureFlash: true // allow flash messages
});