var multer = require('multer');
var controller = require('./controller/controller.js');
var isLoggedIn = controller.isLoggedIn;
var express = require('express');
// var app = express();
const { authenticate } = require('passport');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + file.originalname)
    }
});
var route = express('router');
var upload = multer({ storage: storage })
module.exports = function (app) {
    app.route('/').get(controller.index);
    app.route('/login').get(controller.login).post(controller.passlogin);
    app.route('/signup').get(controller.signup).post(controller.passsignup);
    app.route('/profile').get(isLoggedIn, controller.profile);
    app.route('/admin').get(isLoggedIn, controller.admin);
    app.route('/logout').get(controller.logout);
};
